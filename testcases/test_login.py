import unittest
from selenium import webdriver
from po.navbar import NavBar

#继承unittest.TestCase
class TestLogin(unittest.TestCase):

    @classmethod
    def setUpClass(cls) -> None:
        """
        运行所有的用例之前执行
        :return:
        """
        # 1，打开浏览器
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls) -> None:
        """
        所有的用例执行之后运行
        :return:
        """
        cls.driver.quit()
    def setUp(self) -> None:
        """
        每一个case执行之前的操作
        :return:
        """
        self.driver.delete_all_cookies()
        # 2,打开网站首页
        self.driver.get("http://39.107.96.138:3000/")


    def tearDown(self) -> None:
        """
         每一个case运行之后的操作
        :return:
        """
        #将截图文件放在screenshots目录下，要求如下：
        # 1.以当前日期为截图的文件名 2020--5-17_14_32_29.png
        # 2，图片格式为png
        self.driver.save_screenshot('screenshot\2.png')


    def test_login(self):
        """
        测试用户登录
        :return:
        """
        # 3,导航到登录页面
        navBar=NavBar(self.driver)
        loginPage=navBar.go_to_login_link_page()
        # 4,使用正确的用户名和密码
        loginPage.login_with_username_password(username="imtest11",password="123456")
        # 5.1，登录成功，期望页面能够跳转到首页
        url=self.driver.current_url
        self.assertEqual(url,"http://39.107.96.138:3000/")
        #5.2,登录成功，期望首页页面上有IMtest11用户名
        username=navBar.user_name_text
        # print("username",username)
        self.assertEqual(username,'imtest11')


if __name__ == '__main__':
    unittest.main(verbosity=2)#verbosity设置日志级别