import unittest
from po.navbar import NavBar
from selenium import webdriver


class TestRegister(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        """
        运行所有的用例之前执行
        :return:
        """
        # 1，打开浏览器
        cls.driver = webdriver.Chrome()

    @classmethod
    def tearDownClass(cls) -> None:
        """
        所有的用例执行之后运行
        :return:
        """
        cls.driver.quit()
    def setUp(self) -> None:
        """
        每一个case执行之前的操作
        :return:
        """
        self.driver.delete_all_cookies()
        # 2,打开网站首页
        self.driver.get("http://39.107.96.138:3000/")


    def tearDown(self) -> None:
        """
         每一个case运行之后的操作
        :return:
        """
        #将截图文件放在screenshots目录下，要求如下：
        # 1.以当前日期为截图的文件名 2020--5-17_14_32_29.png
        # 2，图片格式为png
        self.driver.save_screenshot('screenshot\2.png')

    def test_register(self):
        """
        测试用户注册
        :return:
        """

        # 3,导航到注册页面
        navBar = NavBar(self.driver)
        register_page = navBar.go_to_registe_link_page()
        # 4,输入注册内容
        register_page.registe_with_username_pass_repass_email(username="xiaming123", password="123456",
                                                              repasswd="123456", email="673200619@qq.com")
        # 5,注册成功显示信息

        register_text = register_page.success_tip_text
        self.assertEqual(register_text, "欢迎加入 Nodeclub！我们已给您的注册邮箱发送了一封邮件，请点击里面的链接来激活您的帐号。")

