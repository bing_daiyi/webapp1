from selenium.webdriver.common.by import By
from po.base_page import BasePage
from po.login_page import LoginPage
from po.register_page import RegistePage

from selenium import webdriver

class NavBar(BasePage):

    _index_link=By.CSS_SELECTOR,'[href="/"]'
    _unread_link=By.CSS_SELECTOR,'[href="/my/messages"]'
    _newman_link=By.CSS_SELECTOR,'[href="/getstart"]'
    _login_link=By.CSS_SELECTOR,'[href="/signin"]'
    _API_link=By.CSS_SELECTOR,'[href="/api"]'
    _registe_link=By.CSS_SELECTOR,'[href="/signup"]'
    _user_name_link=By.CSS_SELECTOR,'span[class="user_name"] > a.dark'
    _success_tips_text=By.CSS_SELECTOR,'div[class="alert alert-success"]'

    def go_to_index_page(self):
        self._driver.find_element(*self._index_link).click()

    def go_to_unread_page(self):
        self._driver.find_element(*self._unread_link).click()

    def go_to_newman_page(self):
        self._driver.find_element(*self._newman_link).click()

    def go_to_API_link_page(self):
        self._driver.find_element(*self._API_link).click()

    def go_to_login_link_page(self):
        self._driver.find_element(*self._login_link).click()
        return LoginPage(self._driver)

    def go_to_registe_link_page(self):
        self._driver.find_element(*self._registe_link).click()
        return RegistePage (self._driver)
    def go_to_user_center(self):
        self._driver.find_element(*self._user_name_link).click()

    @property
    def user_name_text(self):
        return self._driver.find_element(*self._user_name_link).text

    @property
    def success_tip_text(self):
        return self._driver.find_element(*self._success_tips_text).text


if __name__ == '__main__':
    driver=webdriver.Chrome()
    driver.implicitly_wait(6)
    navbar=NavBar(driver)
    driver.get("http://39.107.96.138:3000/")
    navbar.go_to_newman_page()
    navbar.go_to_index_page()