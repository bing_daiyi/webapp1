from selenium import webdriver
from selenium.webdriver.common.by import By

from po.base_page import BasePage


class LoginPage(BasePage):
    _name_input=By.ID,"name"
    _password_input=By.ID,"pass"
    _login_btn=By.CSS_SELECTOR,'[value="登录"]'

    def login_with_username_password(self,username,password):
        self._driver.find_element(*self._name_input).send_keys(username)
        self._driver.find_element(*self._password_input).send_keys(password)
        self._driver.find_element(*self._login_btn).click()



