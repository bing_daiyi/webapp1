from selenium import webdriver
from selenium.webdriver.common.by import By

from po.base_page import BasePage
class RegistePage(BasePage):
    _name_input=By.ID,"loginname"
    _password_input=By.ID,'pass'
    _re_password_input=By.ID,'re_pass'
    _email_input=By.ID,'email'
    _registe_btn=By.CSS_SELECTOR,'[value="注册"]'
    _error_tip_text=By.CSS_SELECTOR,'[class="alert alert-error"]'

    def registe_with_username_pass_repass_email(self,username,password,repasswd,email):
        self._driver.find_element(*self._name_input).send_keys(username)
        self._driver.find_element(*self._password_input).send_keys(password)
        self._driver.find_element(*self._re_password_input).send_keys(repasswd)
        self._driver.find_element(*self._email_input).send_keys(email)
        self._driver.find_element(*self._registe_btn).click()

    def get_error_tips_test(self):
        return self._driver.find_element(*self._error_tip_text).text