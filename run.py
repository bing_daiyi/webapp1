"""
测试用例集
组织测试用例
"""
import unittest
from testcases.test_login import  TestLogin
from testcases.test_register import TestRegister


def suite():
  testsuite=  unittest.TestSuite()
  # addTest  方法添加单个用例:类名（‘方法名’）
  #使用addtest的方式只适合小型测试场景，同时运行很多个测试用例就不适合
  testsuite.addTest(TestLogin('test_login'))
  testsuite.addTest(TestRegister('test_register'))

  return testsuite

if __name__ == '__main__':
     runner=unittest.TextTestRunner()#用例执行器
     suite=suite()
     runner.run(suite)#运行测试套件